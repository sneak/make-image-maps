# Make Image Maps

A modified version of the [Sketch Image Mapper](https://github.com/maxgillett/sketch-image-mapper) plugin by Max Gillett, which creates HTML image maps directly from Sketch artboards.

## About

This plugin behaves in the same way as [the original](https://github.com/maxgillett/sketch-image-mapper) and exports images and markup required for an image map. The modifications allow for the exported markup to be imported to the [Responsive Image Mapper](http://rim.sneak.co.nz) tool. This tool converts an image map into markup using `<a/>` elements positioned over the top of the image, with percentage based dimensions that allow for responsive scaling.

The image map output will look something like this:

```
<img src="1.jpg" usemap="map-1" width="625" height="740">
<map id="map-1" name="map-1">
	<area shape="rect" alt="Link One" title="Link One" coords="52,61,212,221" href="#" target="_blank" />
	<area shape="rect" alt="Link Two" title="Link Two" coords="320,78,406,195" href="#" target="_blank" />
	<area shape="rect" alt="Link Three" title="Link Three" coords="19,325,163,364" href="#" target="_blank" />
	...
</map>
```

By default the image map links will have `target="_blank"` applied, as this suited my needs best. Remember to remove these if they are not required.

## Installation

1. Download and save "Make Image Maps.sketchplugin"
2. Double-click the downloaded file. This will import the plugin to Sketch and add a 'Make Image Maps' option to the Plugins menu.

## Usage

1. Prepend "&" to the title of each layer (or group) that you wish to generate a clickable region for. Note that the title will be used for the `alt` and `title` attributes.
2. Select "Make Image Maps" from the plugin menu bar.
3. Open the generated HTML files and modify the hyperlinks.
4. Optionally, convert the markup for responsive use with the [Responsive Image Mapper](http://rim.sneak.co.nz) tool.

# Known Issues

The original plugin had some issues when many layers are used (400+) and may cause Sketch to crash. I have not spent any time investigating this issue, and must assume the issue may still exist.

# To-Do

- Add an option to export alernative percentage-based markup without having to use the [Responsive Image Mapper](http://rim.sneak.co.nz) tool.